package com.agh.cms.workregisters.context.dto.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = WorkRegisterRequestValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface WorkRegisterRequestValid {

    String message() default "End date must be after start date and dates must be the same day";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
