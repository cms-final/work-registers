package com.agh.cms.workregisters.context.dto;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
public final class UserRegisterRequest {

    @NotNull(message = "hours per month can't be null")
    public Integer hoursPerMonth;
}
