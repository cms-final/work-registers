package com.agh.cms.workregisters.context.dto;

import java.time.LocalDateTime;

public final class WorkRegisterBasicInfo {

    public String username;
    public LocalDateTime startDateTime;
    public LocalDateTime endDateTime;
}
