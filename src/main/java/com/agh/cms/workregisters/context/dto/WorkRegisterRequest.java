
package com.agh.cms.workregisters.context.dto;

import com.agh.cms.workregisters.context.dto.validation.WorkRegisterRequestValid;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Validated
@WorkRegisterRequestValid
public final class WorkRegisterRequest {

    @FutureOrPresent(message = "start date can't be from past")
    @NotNull(message = "start date can't be null")
    public LocalDateTime startDateTime;

    @FutureOrPresent(message = "end date can't be from past")
    @NotNull(message = "end date can't be null")
    public LocalDateTime endDateTime;
}
