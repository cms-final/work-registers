package com.agh.cms.workregisters.context.dto.validation;

import com.agh.cms.workregisters.context.dto.WorkRegisterRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public final class WorkRegisterRequestValidator implements ConstraintValidator<WorkRegisterRequestValid, WorkRegisterRequest> {

    @Override
    public void initialize(WorkRegisterRequestValid constraintAnnotation) {
    }

    @Override
    public boolean isValid(WorkRegisterRequest request, ConstraintValidatorContext context) {
        return request.endDateTime.isAfter(request.startDateTime) && request.startDateTime.toLocalDate().equals(request.endDateTime.toLocalDate());
    }
}
