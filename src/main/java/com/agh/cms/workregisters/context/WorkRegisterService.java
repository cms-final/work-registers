package com.agh.cms.workregisters.context;

import com.agh.cms.common.domain.UserBasicInfo;
import com.agh.cms.common.domain.dto.ReportCreateRequest;
import com.agh.cms.common.domain.dto.ReportType;
import com.agh.cms.common.domain.exception.ResourceNotFoundException;
import com.agh.cms.workregisters.context.dto.UserRegisterRequest;
import com.agh.cms.workregisters.context.dto.WorkRegisterBasicInfo;
import com.agh.cms.workregisters.context.dto.WorkRegisterRequest;
import com.agh.cms.workregisters.domain.*;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Service
class WorkRegisterService {

    private final UserRepository userRepository;
    private final WorkRegistryRepository workRegistryRepository;
    private final EventRepository eventRepository;
    private final UserBasicInfo userBasicInfo;
    private final ReportRepository reportRepository;

    WorkRegisterService(UserRepository userRepository, WorkRegistryRepository workRegistryRepository, EventRepository eventRepository,
                        UserBasicInfo userBasicInfo, ReportRepository reportRepository) {
        this.userRepository = userRepository;
        this.workRegistryRepository = workRegistryRepository;
        this.eventRepository = eventRepository;
        this.userBasicInfo = userBasicInfo;
        this.reportRepository = reportRepository;
    }

    Duration userTimeToWorkLeft() {
        UserInfo userInfo = userRepository.findById(userBasicInfo.username())
                .orElseThrow(() -> new ResourceNotFoundException(userBasicInfo.username() + " not found"));
        return userInfo.timeToWorkLeft();
    }

    void registerUser(UserRegisterRequest request) {
        UserInfo userInfo = new UserInfo(userBasicInfo.username(), request.hoursPerMonth);
        userRepository.save(userInfo);
    }

    WorkRegisterBasicInfo registerWork(WorkRegisterRequest request) {
        UserInfo userInfo = userRepository.findById(userBasicInfo.username())
                .orElseThrow(() -> new ResourceNotFoundException(userBasicInfo.username() + " not found"));
        WorkRegistry workRegistry = new WorkRegistry(userInfo, request.startDateTime, request.endDateTime);
        eventRepository.createEvent(workRegistry.event());
        workRegistryRepository.save(workRegistry);
        return workRegistry.basicInfo();
    }

    byte[] createReport() {
        ReportCreateRequest request = new ReportCreateRequest();
        request.worker = userBasicInfo.username();
        request.reportType = ReportType.WORKING_DAYS;
        List<WorkRegistry> vacations = workRegistryRepository.findByUserInfo_Username(userBasicInfo.username());
        request.reportRows = vacations.stream()
                .map(WorkRegistry::reportRow)
                .collect(Collectors.toList());
        return reportRepository.createReport(request);
    }
}
