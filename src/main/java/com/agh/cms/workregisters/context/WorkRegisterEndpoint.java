package com.agh.cms.workregisters.context;

import com.agh.cms.workregisters.context.dto.UserRegisterRequest;
import com.agh.cms.workregisters.context.dto.WorkRegisterBasicInfo;
import com.agh.cms.workregisters.context.dto.WorkRegisterRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Duration;

@CrossOrigin
@RestController
@RequestMapping("/workregisters")
class WorkRegisterEndpoint {

    private final WorkRegisterService service;

    WorkRegisterEndpoint(WorkRegisterService service) {
        this.service = service;
    }

    @PostMapping("/userRegistration")
    HttpStatus registerUser(@Valid @RequestBody UserRegisterRequest request) {
        service.registerUser(request);
        return HttpStatus.OK;
    }

    @GetMapping("/timeToWorkLeft")
    ResponseEntity<Duration> timeToWorkLeft() {
        return ResponseEntity.ok(service.userTimeToWorkLeft());
    }

    @PostMapping
    ResponseEntity<WorkRegisterBasicInfo> registerWork(@Valid @RequestBody WorkRegisterRequest request) {
        return ResponseEntity.ok(service.registerWork(request));
    }

    @GetMapping("/report")
    ResponseEntity<byte[]> createReport() {
        byte[] report = service.createReport();
        return ResponseEntity.ok(report);
    }
}
