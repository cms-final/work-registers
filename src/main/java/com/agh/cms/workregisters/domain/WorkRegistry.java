package com.agh.cms.workregisters.domain;

import com.agh.cms.common.domain.EventType;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import com.agh.cms.common.domain.dto.ReportRow;
import com.agh.cms.workregisters.context.dto.WorkRegisterBasicInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Objects;

@Entity
@Access(AccessType.FIELD)
@Table(schema = "work_registers", name = "work_register")
public class WorkRegistry implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "username")
    private UserInfo userInfo;

    @Column(name = "start_date_time")
    private LocalDateTime startDateTime;

    @Column(name = "end_date_time")
    private LocalDateTime endDateTime;

    private WorkRegistry() {
    }

    public WorkRegistry(UserInfo userInfo, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.userInfo = userInfo;
        this.startDateTime = startDateTime.withSecond(0);
        this.endDateTime = endDateTime.withSecond(0);
    }

    public WorkRegisterBasicInfo basicInfo() {
        WorkRegisterBasicInfo workRegisterBasicInfo = new WorkRegisterBasicInfo();
        workRegisterBasicInfo.username = userInfo.username();
        workRegisterBasicInfo.startDateTime = startDateTime;
        workRegisterBasicInfo.endDateTime = endDateTime;
        return workRegisterBasicInfo;
    }

    public EventCreateRequest event() {
        EventCreateRequest event = new EventCreateRequest();
        event.title = "Working day";
        event.type = EventType.WORKING_DAY;
        event.startDate = startDateTime;
        event.endDate = endDateTime;
        event.users = Collections.singleton(userInfo.username());
        event.groups = Collections.emptySet();
        return event;
    }

    public ReportRow reportRow() {
        ReportRow reportRow = new ReportRow();
        reportRow.from = startDateTime.toString();
        reportRow.to = endDateTime.toString();
        return reportRow;
    }

    public Duration workedTime() {
        return Duration.between(startDateTime, endDateTime);
    }

    public boolean isInCurrentMonth() {
        return LocalDate.now().getYear() == startDateTime.getYear() && LocalDate.now().getMonth().equals(startDateTime.getMonth());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkRegistry that = (WorkRegistry) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "WorkRegistry{" +
                "id=" + id +
                '}';
    }
}
