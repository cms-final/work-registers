package com.agh.cms.workregisters.domain;

import com.agh.cms.common.domain.dto.ReportCreateRequest;

public interface ReportRepository {

    byte[] createReport(ReportCreateRequest reportCreateRequest);
}
