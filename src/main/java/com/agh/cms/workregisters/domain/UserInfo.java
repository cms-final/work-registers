package com.agh.cms.workregisters.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Duration;
import java.util.Objects;
import java.util.Set;

@Entity
@Access(AccessType.FIELD)
@Table(schema = "work_registers", name = "user_info")
public class UserInfo implements Serializable {

    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "hours_per_month")
    private Integer hoursPerMonth;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "userInfo")
    private Set<WorkRegistry> workRegistries;

    private UserInfo() {
    }

    public UserInfo(String username, Integer hoursPerMonth) {
        this.username = username;
        this.hoursPerMonth = hoursPerMonth;
    }

    public String username() {
        return username;
    }

    public Duration timeToWorkLeft() {
        Duration workedTime = workRegistries.stream()
                .filter(WorkRegistry::isInCurrentMonth)
                .map(WorkRegistry::workedTime)
                .reduce(Duration::plus)
                .orElse(Duration.ZERO);
        return Duration.ofHours(hoursPerMonth).minus(workedTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo userInfo = (UserInfo) o;
        return Objects.equals(username, userInfo.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public String toString() {
        return "UserInfo{" + "username='" + username + '\'' + '}';
    }
}
