package com.agh.cms.workregisters.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkRegistryRepository extends JpaRepository<WorkRegistry, Integer> {

    List<WorkRegistry> findByUserInfo_Username(String username);
}
