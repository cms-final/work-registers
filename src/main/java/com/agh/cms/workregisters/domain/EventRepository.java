
package com.agh.cms.workregisters.domain;

import com.agh.cms.common.domain.dto.EventBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import org.springframework.web.bind.annotation.RequestBody;

public interface EventRepository {

    EventBasicInfo createEvent(EventCreateRequest newEvent);
}
