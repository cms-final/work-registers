package com.agh.cms.workregisters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.agh.cms")
@EntityScan(basePackages = "com.agh.cms")
@EnableFeignClients
@EnableDiscoveryClient
public class WorkRegistersApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkRegistersApplication.class, args);
    }

}

