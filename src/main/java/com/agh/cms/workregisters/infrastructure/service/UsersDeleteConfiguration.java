package com.agh.cms.workregisters.infrastructure.service;

import com.agh.cms.common.infrastructure.service.UserDeleteEventConsumerFactory;
import com.agh.cms.workregisters.domain.UserRepository;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Configuration
class UsersDeleteConfiguration {

    UsersDeleteConfiguration(UserDeleteEventConsumerFactory consumerFactory, UserRepository userRepository) throws IOException {
        UserDeleteEventConsumer consumer = new UserDeleteEventConsumer(consumerFactory.getChannel(), userRepository);
        consumerFactory.createConsumer(consumer);
    }

    private static final class UserDeleteEventConsumer extends DefaultConsumer {

        private final UserRepository userRepository;

        private UserDeleteEventConsumer(Channel channel, UserRepository userRepository) {
            super(channel);
            this.userRepository = userRepository;
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
            String username = new String(body, StandardCharsets.UTF_8);
            if (userRepository.existsById(username)) {
                userRepository.deleteById(username);
            }
        }
    }
}
