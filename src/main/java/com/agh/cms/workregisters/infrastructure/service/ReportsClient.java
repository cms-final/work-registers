package com.agh.cms.workregisters.infrastructure.service;

import com.agh.cms.common.domain.dto.ReportCreateRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("reports")
interface ReportsClient {

    @PostMapping("/reports")
    ResponseEntity<byte[]> createReport(@RequestBody ReportCreateRequest request);
}
