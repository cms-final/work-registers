package com.agh.cms.workregisters.infrastructure.service;

import com.agh.cms.common.domain.dto.EventBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("events")
interface EventsClient {

    @PostMapping("/events")
    ResponseEntity<EventBasicInfo> createEvent(@RequestBody EventCreateRequest request);
}
