package com.agh.cms.workregisters.infrastructure.service;

import com.agh.cms.common.domain.dto.ReportCreateRequest;
import com.agh.cms.workregisters.domain.ReportRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
class ReportRestRepository implements ReportRepository {

    private final ReportsClient reportsClient;

    ReportRestRepository(ReportsClient reportsClient) {
        this.reportsClient = reportsClient;
    }

    @Override
    public byte[] createReport(ReportCreateRequest reportCreateRequest) {
        ResponseEntity<byte[]> report = reportsClient.createReport(reportCreateRequest);
        return report.getBody();
    }
}
