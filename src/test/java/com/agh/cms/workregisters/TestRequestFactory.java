package com.agh.cms.workregisters;

import com.agh.cms.workregisters.context.dto.WorkRegisterRequest;

import java.time.LocalDateTime;

public final class TestRequestFactory {

    private TestRequestFactory() {
    }

    public static WorkRegisterRequest workRegisterRequest() {
        WorkRegisterRequest request = new WorkRegisterRequest();
        request.startDateTime = LocalDateTime.now().plusHours(1);
        request.endDateTime = LocalDateTime.now().plusHours(2);
        return request;
    }
}
