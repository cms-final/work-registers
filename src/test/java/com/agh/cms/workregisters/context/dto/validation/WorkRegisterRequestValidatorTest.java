package com.agh.cms.workregisters.context.dto.validation;

import com.agh.cms.workregisters.context.dto.WorkRegisterRequest;
import org.hibernate.validator.internal.engine.constraintvalidation.ConstraintViolationCreationContext;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.validation.ConstraintValidatorContext;

import static com.agh.cms.workregisters.TestRequestFactory.workRegisterRequest;
import static org.junit.Assert.*;


public class WorkRegisterRequestValidatorTest {

    private final WorkRegisterRequestValidator validator;

    @MockBean
    private ConstraintValidatorContext constraintValidatorContext;

    public WorkRegisterRequestValidatorTest() {
        validator = new WorkRegisterRequestValidator();
    }

    @Test
    public void isValid_whenEndDateTimeIsNotAfterStartDateTime_returnsFalse() {
        WorkRegisterRequest request = workRegisterRequest();
        request.endDateTime = request.startDateTime.minusHours(3);

        boolean result = validator.isValid(request, constraintValidatorContext);
        
        assertFalse(result);
    }

    @Test
    public void isValid_whenDatesDoNotEqual_returnsFalse() {
        WorkRegisterRequest request = workRegisterRequest();
        request.endDateTime = request.endDateTime.plusDays(1);

        boolean result = validator.isValid(request, constraintValidatorContext);

        assertFalse(result);
    }

    @Test
    public void isValid_whenRequestIsCorrect_returnsTrue() {
        WorkRegisterRequest request = workRegisterRequest();

        boolean result = validator.isValid(request, constraintValidatorContext);

        assertTrue(result);
    }
}