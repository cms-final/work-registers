package com.agh.cms.workregisters.context;

import com.agh.cms.common.domain.UserBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import com.agh.cms.workregisters.EventsFeignException;
import com.agh.cms.workregisters.context.dto.WorkRegisterBasicInfo;
import com.agh.cms.workregisters.context.dto.WorkRegisterRequest;
import com.agh.cms.workregisters.domain.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import feign.RetryableException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static com.agh.cms.workregisters.TestRequestFactory.workRegisterRequest;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.main.allow-bean-definition-overriding=true"})
@AutoConfigureMockMvc
public class RegisterWorkIntTest {

    private static final String WORK_REGISTERS = "/workregisters";
    private final ObjectMapper mapper;
    private final UserInfo correctUserInfo;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private WorkRegistryRepository workRegistryRepository;

    @MockBean
    private EventRepository eventRepository;

    @MockBean
    private UserBasicInfo userBasicInfo;

    public RegisterWorkIntTest() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        correctUserInfo = new UserInfo("user", 100);
    }

    @Before
    public void setUp() throws Exception {
        when(userBasicInfo.username()).thenReturn("user");
    }

    @Test
    public void registerWork_whenRequestBodyIsInvalid_returns400() throws Exception {
        WorkRegisterRequest request = workRegisterRequest();
        request.endDateTime = request.endDateTime.plusDays(1);

        mockMvc.perform(MockMvcRequestBuilders.post(WORK_REGISTERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"code\":\"400\",\"message\":\"JSON not valid\"}"));
    }

    @Test
    public void registerWork_whenUserDoesNotExist_returns404() throws Exception {
        when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.post(WORK_REGISTERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(workRegisterRequest())))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"code\":\"404\",\"message\":\"user not found\"}"));
        verify(workRegistryRepository, never()).save(any(WorkRegistry.class));
    }

    @Test
    public void registerWork_whenEventsServiceIsNotAvailable_returns503() throws Exception {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(correctUserInfo));
        when(eventRepository.createEvent(any(EventCreateRequest.class))).thenThrow(RetryableException.class);

        mockMvc.perform(MockMvcRequestBuilders.post(WORK_REGISTERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(workRegisterRequest())))
                .andExpect(status().isServiceUnavailable())
                .andExpect(content().string("{\"code\":\"503\",\"message\":\"Request timed out\"}"));
        verify(workRegistryRepository, never()).save(any(WorkRegistry.class));
    }

    @Test
    public void registerWork_whenUserHasEventAtThisTime_returns400() throws Exception {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(correctUserInfo));
        when(eventRepository.createEvent(any(EventCreateRequest.class))).thenThrow(new EventsFeignException());

        mockMvc.perform(MockMvcRequestBuilders.post(WORK_REGISTERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(workRegisterRequest())))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"code\":\"400\",\"message\":\"user has event at this time\"}"));
        verify(workRegistryRepository, never()).save(any(WorkRegistry.class));
    }

    @Test
    public void registerWork_whenOperationIsSuccessful_returns200() throws Exception {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(correctUserInfo));
        WorkRegisterRequest request = workRegisterRequest();
        WorkRegistry workRegistry = new WorkRegistry(correctUserInfo, request.startDateTime, request.endDateTime);
        WorkRegisterBasicInfo basicInfo = workRegistry.basicInfo();

        mockMvc.perform(MockMvcRequestBuilders.post(WORK_REGISTERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(basicInfo)))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(basicInfo)));
    }
}
