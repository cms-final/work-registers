package com.agh.cms.workregisters;

import feign.FeignException;

public class EventsFeignException extends FeignException {

    public EventsFeignException() {
        super("status 400 reading EventsClient#createEvent(EventCreateRequest); content:\n" +
                "{\"code\":\"400\",\"message\":\"user has event at this time\"}");
    }
}
